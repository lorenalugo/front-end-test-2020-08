# Prueba Técnica - Programador (Front-end)

## INTRODUCCIÓN
Este repositorio contiene una serie de requerimientos de un Caso Práctico, que busca evaluar las capacidades técnicas del candidato con respecto a las principales funciones y responsabilidades que se requieren dentro del área de Tecnología.

#### ¿Qué se busca evaluar?
Se evaluarán los siguientes aspectos:
  + Creatividad para resolver los requerimientos
  + Calidad del código entregado (estructura y buenas prácticas)
  + Eficiencia de los algoritmos entregados
  + Familiaridad con Frameworks de desarrollo Web.

## IMPORTANTE
1. Se solicita crear la aplicación utilizando la tecnología Web de su elección, se privilegiarán a los candidatos que utilicen algún framework moderno de desarrollo web (Vue, React, Angular).
2. Se requiere de una cuenta en **Gitlab** y **Figma** para realizar este ejercicio.
5. **Antes de comenzar a programar:**
    * Realizar un `Fork` privado de este repositorio (https://gitlab.com/check2/front-end-test-2020-08).
    * Clonar el fork privado a su máquina local  `git clone git@github.com:USERNAME/FORKED-PROJECT.git`
6. **Al finalizar**, **notificar y compartir su fork a los siguientes correos:**
    * [miguel@check.pe](mailto:miguel@check.pe)
    * [jhoel@check.pe](mailto:jhoel@check.pe)

## EJERCICIO PRÁCTICO
**Objetivo:** Crear una aplicación que obtenga una lista de cursos, explotando el API Rest del servidor staging de prendea https://api-staging.prendea.com/api/courses/.


#### Requerimientos generales

1. La aplicación debe cumplir con los siguientes **requisitos funcionales:**

    - Crear una aplicación que incluya una sola vista donde se pueda visualizar los cursos ofrecidos en el API.

    - La vista creada debe ser creada a partir del siguiente figma: https://www.figma.com/file/O8uLJoQ4qPJk647TmaAnvt/Prendea-Test?node-id=0%3A1.

    - Cada card representa un curso, el nombre/imagen que se encuentra en la parte inferior de cada card corresponde al profesor de dicho course.

    - Cada card debe ser clickeable y redirigir a https://staging.prendea.com/course/<course_id>/prendea-test según corresponda

    - La palabra **Inicio** que se encuentra en la parte superior debe redirigir a https://staging.prendea.com

    - Se debe incluir alguna forma de páginado aprovechando el API (scroll, páginas, etc), aquí hay total libertad para agregar algo al diseño original.

#### Descripción del API

La url del API es: https://api-staging.prendea.com/api/courses/?limit=10&offset=10, donde **limit** es el número de resultados por página y **offset** es el número de elementos saltados por el api. Por ejemplo un consulta a https://api-staging.prendea.com/api/courses/?limit=10&offset=12 retorna los elementos del 13 al 22 (10 por página indicados por limit=10 y saltando 12 elementos indicados por offset=10).

- **count:** número total de cursos
- **next:** próxima página de resultados
- **previous:** anterior página de resultados
- **results.id:** id del curso
- **results.title:** título del curso
- **results.picture:** imagen del curso
- **results.min_age:** edad mínima para el curso
- **results.max_age:** edad máxima para el curso
- **results.next_lecture_date:** fecha de la próxima lección (la que se muestra en el card, esta debe ser formateada adecuadamente)
- **results.teacher.full_name:** nombre del profesor
- **results.teacher.public_picture:** imagen del profesor


